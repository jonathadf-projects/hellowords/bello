# bello

A program written in a raw interpreted programming language. It demonstrates when the source code does not need to be built, but only needs to be installed.

# Repository

<a href="https://packagecloud.io/"><img height="46" width="158" alt="Private NPM registry and Maven, RPM, DEB, PyPi and RubyGem Repository · packagecloud" src="https://packagecloud.io/images/packagecloud-badge.png" /></a>

